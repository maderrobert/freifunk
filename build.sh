#!/bin/bash
set -e
set -u

CLIENTMAX=2

cd ../OpenWrt-ImageBuilder-x86-64.Linux-x86_64

for i in `seq 0 $CLIENTMAX`; do

#PACKAGES='oonf-olsrd2 ip6tables-mod-nat ip tcpdump nano htop tmux -kmod-acpi-button'
#PACKAGES='babeld ip tcpdump nano -kmod-acpi-button'
#PACKAGES='bmx6 bmx6-uci-config ip tcpdump nano htop -kmod-acpi-button'

make image PACKAGES='oonf-olsrd2 ip6tables-mod-nat ip tcpdump nano htop tmux -kmod-acpi-button'  FILES=../git/image-builder/files$i -j5
cd bin/x86/
gunzip openwrt-x86-64-combined-ext4.img.gz
mv openwrt-x86-64-combined-ext4.img ../../../images/openwrt-x86-64-combined-ext4-$i.img
cd ../../
done
