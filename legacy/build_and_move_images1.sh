#!/bin/bash
cd ../OpenWrt-ImageBuilder-15.05-x86-generic.Linux-x86_64
make image PACKAGES='oonf-olsrd2 ip tcpdump nano'  FILES=../git/image-builder/files
cd bin/x86
gunzip openwrt-15.05-x86-generic-combined-ext4.img.gz
mv openwrt-15.05-x86-generic-combined-ext4.img ../../../
cd ../../../
cp openwrt-15.05-x86-generic-combined-ext4.img openwrt-15.05-x86-generic-combined-ext4-0.img
cp openwrt-15.05-x86-generic-combined-ext4.img openwrt-15.05-x86-generic-combined-ext4-1.img
cp openwrt-15.05-x86-generic-combined-ext4.img openwrt-15.05-x86-generic-combined-ext4-2.img
cp openwrt-15.05-x86-generic-combined-ext4.img openwrt-15.05-x86-generic-combined-ext4-3.img
