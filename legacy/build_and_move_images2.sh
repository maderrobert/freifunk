#!/bin/bash
cd ../OpenWrt-ImageBuilder-x86-64.Linux-x86_64
make image PACKAGES='oonf-olsrd2 ip tcpdump nano -kmod-acpi-button'  FILES=../git/image-builder/files
cd bin/x86
gunzip openwrt-x86-64-combined-ext4.img.gz
mv openwrt-x86-64-combined-ext4.img ../../../
cd ../../../
cp openwrt-x86-64-combined-ext4.img openwrt-x86-64-combined-ext4-0.img
cp openwrt-x86-64-combined-ext4.img openwrt-x86-64-combined-ext4-1.img
cp openwrt-x86-64-combined-ext4.img openwrt-x86-64-combined-ext4-2.img
cp openwrt-x86-64-combined-ext4.img openwrt-x86-64-combined-ext4-3.img
