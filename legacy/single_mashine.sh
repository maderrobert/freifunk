#!/bin/bash
#------------- config area --------------
IMAGE='../openwrt-15.05-x86-generic-combined-ext4.img'


#----------- end config area ------------

qemu-system-x86_64 $IMAGE \
  -netdev bridge,br=link0,id=hn0 \
  -device e1000,netdev=hn0,id=nic1 \
  -netdev bridge,br=link1,id=hn1 \
  -device e1000,netdev=hn1,id=nic2 \
  -nographic
