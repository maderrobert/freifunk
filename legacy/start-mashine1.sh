#!/bin/bash
#------------- config area --------------
IMAGE0='../openwrt-15.05-x86-generic-combined-ext4-0.img'
#IMAGE0='../openwrt-x86-64-combined-ext4-0.img'
IMAGE1='../openwrt-15.05-x86-generic-combined-ext4-1.img'
#IMAGE1='../openwrt-x86-64-combined-ext4-1.img'
IMAGE2='../openwrt-15.05-x86-generic-combined-ext4-2.img'
#IMAGE2='../openwrt-x86-64-combined-ext4-2.img'
IMAGE3='../openwrt-15.05-x86-generic-combined-ext4-3.img'
#IMAGE3='../openwrt-x86-64-combined-ext4-3.img'
#DEVDRIVER='virtio'
DEVDRIVER='e1000'

#----------- end config area ------------

if [ $# -eq 0 ]
  then
    echo "Valid arguments: 1, 2, 3 ..."
    exit
fi

if [ "$1" -eq "1" ];then

qemu-system-x86_64 $IMAGE0 \
  -netdev bridge,br=wan0,id=wanhn0 \
  -device $DEVDRIVER,netdev=wanhn0,id=wannic0,mac=00:a2:1b:b4:00:0a \
  -netdev bridge,br=link0,id=hn0 \
  -device $DEVDRIVER,netdev=hn0,id=nic0,mac=00:a2:1b:b4:d3:0a \
  -netdev bridge,br=link1,id=hn1 \
  -device $DEVDRIVER,netdev=hn1,id=nic1,mac=00:a2:1b:b4:d3:0b \
  -netdev bridge,br=link3,id=hn2 \
  -device $DEVDRIVER,netdev=hn2,id=nic2,mac=00:a2:1b:b4:d3:1a \
  -nographic
  exit
fi

if [ "$1" -eq "2" ];then

qemu-system-x86_64 $IMAGE1 \
  -netdev bridge,br=wan1,id=wanhn0 \
  -device $DEVDRIVER,netdev=wanhn0,id=wannic0,mac=00:a2:1b:b4:00:0b \
  -netdev bridge,br=link0,id=hn0 \
  -device $DEVDRIVER,netdev=hn0,id=nic0,mac=00:a2:1b:b4:d3:0c \
  -netdev bridge,br=link2,id=hn1 \
  -device $DEVDRIVER,netdev=hn1,id=nic1,mac=00:a2:1b:b4:d3:0d \
  -nographic
  exit
fi

if [ "$1" -eq "3" ];then
  
qemu-system-x86_64 $IMAGE2 \
  -netdev bridge,br=wan2,id=wanhn0 \
  -device $DEVDRIVER,netdev=wanhn0,id=wannic0,mac=00:a2:1b:b4:00:0c \
  -netdev bridge,br=link2,id=hn0 \
  -device $DEVDRIVER,netdev=hn0,id=nic0,mac=00:a2:1b:b4:d3:0e \
  -netdev bridge,br=link1,id=hn1 \
  -device $DEVDRIVER,netdev=hn1,id=nic1,mac=00:a2:1b:b4:d3:0f \
  -nographic
  exit
fi

if [ "$1" -eq "4" ];then
qemu-system-x86_64 $IMAGE3 \
  -netdev bridge,br=wan3,id=wanhn0 \
  -device $DEVDRIVER,netdev=wanhn0,id=wannic0,mac=00:a2:1b:b4:00:0d \
  -netdev bridge,br=link3,id=hn0 \
  -device $DEVDRIVER,netdev=hn0,id=nic0,mac=00:a2:1b:b4:d3:1b \
  -nographic
  exit
fi
