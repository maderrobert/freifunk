#/bin/sh
set -e
set -u

cd ../odhcp6c
VERSION=`git log -1 --format="%H"`

cd ../openwrt
DIRECTORY='build_dir/target-x86_64_musl-1.1.14/odhcp6c-2015-09-04/'
if [ -d "$DIRECTORY" ]; then
	rm -r $DIRECTORY
fi
FILE='dl/odhcp6c-2015-09-04.tar.bz2'
if [ -e "$FILE" ]; then
	rm $FILE
fi

sed -i "18s/.*/PKG_SOURCE_VERSION:=$VERSION/" package/network/ipv6/odhcp6c/Makefile
make package/odhcp6c/prepare
make package/odhcp6c/compile
cd ..
cp openwrt/build_dir/target-x86_64_musl-1.1.14/odhcp6c-2015-09-04/odhcp6c git/image-builder/files2/usr/sbin/odhcp6c
