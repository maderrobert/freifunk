#!/bin/bash
set -u
#--general
BRIDGE_CONF_FILE='/etc/qemu/bridge.conf'
#-----------------
CLEAN=0
if [[ ! -z ${1+x} && "$1" == "clean" ]]; then
	CLEAN=1
fi

function create_interfaces() {
	for i in `seq 0 $2`; do
		DEV="$1$i"
		GREP=`brctl show | grep $DEV`
		
		if [ $CLEAN -eq 1 ];then
			if [ ! -z "${GREP}" ]; then
				echo "removing interface $DEV"
				ip link set $DEV down
				brctl delbr $DEV
			fi
			
			sed -i "/allow $DEV/d" $BRIDGE_CONF_FILE
		else
			if [ -z "${GREP}" ]; then
				echo "creating interface $DEV"
				brctl addbr $DEV
				ip link set $DEV up
			fi
			
			if ! grep -q $DEV $BRIDGE_CONF_FILE; then
				echo "allow $DEV" >> $BRIDGE_CONF_FILE
			fi 
		fi
	done
}

#--links
create_interfaces "link" 3
#--wans
create_interfaces "wan" 1
