#!/bin/bash
set -e
set -u

#------------- config area --------------
#DEVDRIVER='virtio'
DEVDRIVER='e1000'

#----------- end config area ------------

if [ $# -eq 0 ]
  then
    echo "Valid arguments: 0, 1, 2, 3 ..."
    exit
fi

IMAGE="../images/openwrt-x86-64-combined-ext4-$1.img"

########## Gateways ##########

if [ "$1" -eq "0" ];then
qemu-system-x86_64 $IMAGE \
  -netdev bridge,br=wan0,id=wanhn0 \
  -device $DEVDRIVER,netdev=wanhn0,id=wannic0,mac=00:a2:1b:b4:00:0b \
  -netdev bridge,br=link0,id=hn0 \
  -device $DEVDRIVER,netdev=hn0,id=nic0,mac=00:a2:1b:b4:d3:0c \
  -netdev bridge,br=link2,id=hn1 \
  -device $DEVDRIVER,netdev=hn1,id=nic1,mac=00:a2:1b:b4:d3:0d \
  -nographic
  exit
fi

if [ "$1" -eq "1" ];then
 
qemu-system-x86_64 $IMAGE \
  -netdev bridge,br=wan1,id=wanhn0 \
  -device $DEVDRIVER,netdev=wanhn0,id=wannic0,mac=00:a2:1b:b4:00:0c \
  -netdev bridge,br=link2,id=hn0 \
  -device $DEVDRIVER,netdev=hn0,id=nic0,mac=00:a2:1b:b4:d3:0e \
  -netdev bridge,br=link1,id=hn1 \
  -device $DEVDRIVER,netdev=hn1,id=nic1,mac=00:a2:1b:b4:d3:0f \
  -nographic
  exit
fi

########## Node ##########

if [ "$1" -eq "2" ];then
qemu-system-x86_64 $IMAGE \
  -netdev bridge,br=link0,id=hn0 \
  -device $DEVDRIVER,netdev=hn0,id=nic0,mac=00:a2:1b:b4:d3:0a \
  -netdev bridge,br=link1,id=hn1 \
  -device $DEVDRIVER,netdev=hn1,id=nic1,mac=00:a2:1b:b4:d3:0b \
  -netdev bridge,br=link3,id=hn2 \
  -device $DEVDRIVER,netdev=hn2,id=nic2,mac=00:a2:1b:b4:d3:1a \
  -nographic
  exit
fi

########## Clients ##########

if [ "$1" -eq "3" ];then
qemu-system-x86_64 $IMAGE \
  -netdev bridge,br=link3,id=hn0 \
  -device $DEVDRIVER,netdev=hn0,id=nic0,mac=00:a2:1b:b4:d3:2a \
  -nographic
  exit
fi

if [ "$1" -eq "4" ];then
qemu-system-x86_64 $IMAGE \
  -netdev bridge,br=link3,id=hn0 \
  -device $DEVDRIVER,netdev=hn0,id=nic0,mac=00:a2:1b:b4:d3:2b \
  -nographic
  exit
fi
